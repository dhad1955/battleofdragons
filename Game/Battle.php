<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 21/03/2018
 * Time: 23:42
 */

namespace Game;

class Battle
{
    // SECONDS to wait between attacks
    private $battleTime = 3;
    // Who's in the battle
    private $participants = [];
    // Who's turn is it?
    private $turnCaret = 0;
    // Battle active or not flag
    private $battleActive = false;

    public function __construct(DragonGame $game)
    {
        $this->game = $game;
    }

    // Add a participant to the battle
    public function addParticipant(Dragon $dragon)
    {
        $this->participants[] = $dragon;

        $this->game->logf('Battle',
            "%s has entered the battle | HP: %s/%s | Attack: %s | Defence: %s | Luck %s",
                $dragon->getName(),
                $dragon->getCurrentHealth(),
                $dragon->getMaxHealth(),
                $dragon->getStat('attack'),
                $dragon->getStat('defence'),
                $dragon->getStat('luck')
            );
    }

    // Get the dragon who's turn it is
    public function getCurrentAttacker(): Dragon
    {
        return $this->participants[$this->turnCaret];
    }

    // Get the dragon who's defending
    public function getCurrentDefender(): Dragon
    {
        $tmp = $this->turnCaret;

        return $this
            ->participants[(++$tmp) % count($this->participants)];
    }

    // Do the turn
    public function nextTurn()
    {
        // Swap between participants
        $this->turnCaret = (++$this->turnCaret) % count($this->participants);

        $attacker = $this->getCurrentAttacker();
        $defender = $this->getCurrentDefender();

        $this->game
            ->log('Battle', $attacker->getName() . ' is attacking');

        $attackerLuck = rand(0, $attacker->getStat('luck', $defender));
        $defenderLuck = rand(0, $defender->getStat('luck', $attacker));

        $this->game
            ->logf('Battle', "Attacker %s rolled a %d",
                $attacker->getName(),
                $attackerLuck);

        $this->game
            ->logf('Battle', "Defender %s rolled a %d",
                $defender->getName(),
                $defenderLuck);

        // If luck is more than defender luck then its a hit
        if ($attackerLuck > $defenderLuck) {

            $calculateAttack  = $attacker->getStat('attack', $defender);
            $calculateDefence = $defender->getStat('defence', $attacker);

            $damage = rand(0, $calculateAttack - ($calculateDefence / 10)) + 1;

            $defender->hit($damage);

            $this->game
                ->logf('Battle',
                    "Attacker %s has HIT defender %s with damage %d | HP : %d/%d",
                    $attacker->getName(),
                    $defender->getName(),
                    $damage,
                    $defender->getCurrentHealth(),
                    $defender->getMaxHealth());

            if ($defender->isDead()) {

                $this->game
                    ->logf('Battle', "Oh NO! Defender %s has DIED", $defender->getName());

                $this->game
                    ->logf('Battle', "%s is victorious!", $attacker->getName());

                $this->battleActive = false;
            }

        } else {
            $this->game
                ->logf('Battle', "Attacker %s has MISSED defender %s", $attacker->getName(), $defender->getName());
        }
    }

    public function beginBattle()
    {
        $this->battleActive = true;

        while ($this->battleActive) {

            echo PHP_EOL;
            $this->nextTurn();
            echo PHP_EOL;

            sleep($this->battleTime);
        }
    }
}
