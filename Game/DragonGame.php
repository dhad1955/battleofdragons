<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 21/03/2018
 * Time: 23:24
 */

namespace Game;


class DragonGame
{

    private $dragonRepository = NULL;
    private $currentBattle = NULL;

    public function __construct()
    {
        $this->log('Boot', 'Loading dragons');
        $this->dragonRepository = new DragonRepository($this,'./Config');
        $this->beginBattle();
    }

    public function log($entity, $message)
    {
        echo "[{$entity}]: {$message}".PHP_EOL;
    }

    public function logf($entity, $message, ...$args) {
        return $this->log($entity, vsprintf($message, $args));
    }


    public function beginBattle()
    {
        $this->currentBattle = new Battle($this);

        // Add dragon #1
        $this->currentBattle->addParticipant($this
            ->dragonRepository->randomDragon());

        // Add dragon #2
        $this->currentBattle->addParticipant($this
            ->dragonRepository->randomDragon());

        // Start battle
        $this->currentBattle->beginBattle();
    }

}
