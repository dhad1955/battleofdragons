<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 21/03/2018
 * Time: 00:11
 */

namespace Game;


class Dragon
{
    private $definition;
    private $maxHealth = 0;
    private $name;
    private $stats = [];

    public function __construct(string $name, $definition)
    {
        // Name is not in the definition as we may use it later
        // For example we may want to name this dragon dragon1 or dragon2 etc
        $this->name = $name;

        // Store definition incase we want to respawn
        $this->definition = $definition;

        $this->spawn();
    }

    // Spawn / respawn
    // Set stats
    private function spawn()
    {
        // set our random stats
        $this->stats = array_map(function ($result) {
                return rand($result['min'], $result['max']);
        }, $this->definition['stats']);

        // set our max health so we remember it
        $this->maxHealth = $this->stats['health'];
    }

    public function getType()
    {
        return $this->definition['type'];
    }

    public function getCurrentHealth()
    {
        return $this->stats['health'];
    }

    public function getMaxHealth()
    {
        return $this->maxHealth;
    }
    // Get our spawned name
    public function getName()
    {
        return $this->name;
    }

    // Return the bonuses we have for the opposing dragon
    public function getBonuses(Dragon $opponent, $type)
    {
        return array_filter($this->definition['bonuses'], function($bonus) use ($opponent, $type) {
                return $bonus['opponent'] == $opponent->getType() && $bonus['type'] == $type;
        });
    }

    // Take some damage
    public function hit(float $damage) : void
    {
        $this->stats['health'] -= $damage;
        if($this->stats['health'] < 0) {
            $this->stats['health'] = 0;
        }
    }

    // Get a stat (with modifiers for special ability)
    public function getStat($type, $opponent = null)
    {
        if($opponent instanceof Dragon)
            $bonuses = $this->getBonuses($opponent, $type);
        else
            $bonuses = [];

        $stat = $this->stats[$type];
        foreach($bonuses as $bonus) {
            $stat *= (1 + $bonus['boostPercent'] / 100);
        }

        return $this->stats[$type];
    }

    // Are we dead?
    public function isDead() : bool
    {
        return $this->stats['health'] <= 0;
    }
}
