<?php
/**
 * Created by PhpStorm.
 * User: danielh
 * Date: 21/03/2018
 * Time: 23:23
 */

namespace Game;

class DragonRepository
{
    // contents containing our dragons
    private $definitions = [];
    // caret for keeping uniqueness on names
    private $caret = 0;

    public function __construct(DragonGame $game, $path)
    {
        foreach(scandir($path) as $file) {
            // are we a valid dragon file?
            if(preg_match("/(?:\.dragon\.json)/",$file)) {
                // Split file into sections to get the name
                $sections = explode('.', $file);
                $dragonName = $sections[0];
                // store the definition
                $this->definitions[$dragonName] = json_decode(file_get_contents($path.'/'.$file), true);
                $game->log('Boot', "Loaded dragon {$dragonName}");
            }
        }
    }

    // Create a new dragon with a random definition
    public function randomDragon()
    {
        // Dragon names are used as the keys
        $dragonNames = array_keys($this->definitions);

        $randomName = $dragonNames[rand(0, count($dragonNames) - 1)];
        $randomDragon = $this->definitions[$randomName];

        return new Dragon($randomDragon['name']. ' '.(++$this->caret), $this->definitions[$randomName]);
    }

    // Get a dragon
    public function getDragon($key) {

        if(!isset($this->definitions[$key])) {
                throw new \Exception("Dragon not found {$key} ");
        }

        return new Dragon($key, $this->definitions[$key]);
    }
}
